#!/bin/bash
mysql -uroot << HERE
use csv;
update Descriptions set Class = '**Reserved**' where Description like '**Reserved**%';
update Descriptions set Class = 'Email Service' where Description like '%email%';
update Descriptions set Class = 'Database' where Description like '%sql%';
update Descriptions set Class = 'Web Service' where Description like '%xss%';
update Descriptions set Class = 'Local Mem' where Description like '%overflow%';
update Descriptions set Class = 'Local Machine' where Description like '%code execution%';
update Descriptions set Class = 'Local Perms' where Description like '%escalat%';
update Descriptions set Class = 'Web Service' where Description like '%arbitrary%execution%';
HERE
