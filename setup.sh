#!/bin/bash
file=$(pwd)
sudo apt install -y mariadb-client mariadb-server
sudo systemctl start mariadb

mysql -uroot << HERE
create database csv;
use csv;
create table Descriptions(cve varchar(15), Description varchar(5000), Class varchar(1000));
load data local infile '$file/allitems.csv'
into table Descriptions
fields terminated by ','
enclosed by '"'
lines terminated by '\n';

HERE
